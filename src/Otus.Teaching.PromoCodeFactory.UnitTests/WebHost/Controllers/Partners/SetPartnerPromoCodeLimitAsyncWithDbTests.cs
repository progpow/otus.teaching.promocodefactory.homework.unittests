﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.UnitTests.Helpers;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncWithDbTests
    {
        private readonly IRepository<Partner> _partnersRepositoryDb;
        private readonly PartnersController _partnersController;
        
        public SetPartnerPromoCodeLimitAsyncWithDbTests()
        {
            var serviceProvider = InMemoryConfiguration.GetServiceProvider();
            _partnersRepositoryDb = serviceProvider.GetService<IRepository<Partner>>();

            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            fixture.Inject(_partnersRepositoryDb);
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            await
                (await
                    (await new UnitTestBuilder()

                .CreateTest()
                .ArrangeAsync(async () =>
                {
                    await _partnersRepositoryDb.AddAsync(PartnerBuilder.CreateBasePartner().ClearLimits());
                    var test = (await _partnersController.GetPartnersAsync()).Result;
                    return new SetPartnerPromoCodeLimitArrangeContext()
                    {
                        PartnerId = (await _partnersController.GetPartnersAsync()).Result.As<OkObjectResult>().Value.As<List<PartnerResponse>>().First().Id,
                        Request = SetPartnerPromoCodeLimitRequestBuilder.CreateSetPartnerPromoCodeLimitRequestValid()
                    };
                }))
                .ActAsync(async (arrangeContext) => (await _partnersController.SetPartnerPromoCodeLimitAsync(arrangeContext.PartnerId,  arrangeContext.Request))))
                .AssertAsync(async (arrangeContext, actResult) =>
                    {
                        actResult.Should().BeAssignableTo<CreatedAtActionResult>();
                        var partner = await _partnersRepositoryDb.GetByIdAsync(arrangeContext.PartnerId);
                        partner.PartnerLimits.Count.Should().Be(1);
                    }
                );
        }
    }
}